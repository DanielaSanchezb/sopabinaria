/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 * Clase para el manejo de los datos de un Bit
 * @author madar
 */
public class Bit {
    
    private boolean valor;
    
    /**
     * Constructor para crear un Bit con su valor
     * @param valor un dato de tipo boolean que almacena el valor del Bit: true o false
     */
    public Bit(boolean valor) {
        this.valor = valor;
    }
    
    /**
     * Constructor vacío para la clase Bit
     */
    public Bit() {
    }
    
    /**
     * Obtiene el valor del Bit
     * @return un boolean con el valor del Bit
     */
    public boolean isValor() {
        return valor;
    }
    
     /**
     * Actualiza el valor del Bit
     * @param valor un dato de tipo boolean que representa el nuevo valor
     */
    public void setValor(boolean valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        int dato = 0;
        if(valor)
            dato= 1;
        String msg = "" +dato;
        return  msg;
    }
    
    
    
}
