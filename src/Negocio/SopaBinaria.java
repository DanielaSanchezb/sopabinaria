/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Bit;
import Util.LeerSopaExcel;
import java.io.File;
import java.io.IOException;

/**
 * Clase SopaBinaria
 * @author madar
 */
public class SopaBinaria {

    private Bit mySopaBinaria[][];
    private Bit solucionSopaBinaria[][];
    private boolean cuadrada;

    /**
     * Constructor vacío de la clase SopaBinaria
     */
    public SopaBinaria() {
    }
    
    /**
     * Constructor que carga la sopa binaria a partir de un Excel
     * Valida que la sopa binaria sea Cuadrada o Rectangular
     * @param rutaArchivoExcel un String con la ruta y el nombre del archivo
     * @throws IOException genera excepción cuando el archivo no existe
     */
    public SopaBinaria(String rutaArchivoExcel) throws IOException {
        LeerSopaExcel myExcel = new LeerSopaExcel(rutaArchivoExcel, 0);
        String datos[][] = myExcel.getMatriz();
        this.mySopaBinaria = new Bit[datos.length][datos[0].length];
        this.solucionSopaBinaria = new Bit[datos.length][datos[0].length];
        for (int i = 0; i < datos.length; i++) {
            for (int j = 0; j < datos[i].length; j++) {
                if (datos[i][j].equals("1")) {
                    mySopaBinaria[i][j] = new Bit(true);
                } else {
                    mySopaBinaria[i][j] = new Bit(false);
                }
            }
        }
        if (mySopaBinaria.length == mySopaBinaria[0].length) {
            this.cuadrada = true;
        }

    }

    /**
     *Muestra cuantas veces está el decimal horizontalmente en la sopa binaria 
     * @param decimal numero en base 10 que ingresa el usuario el cual será convertido 
     * a binario, para su busqueda horizontal en la sopa binaria
     * @return retorna cuantas veces se encuentra el decimal horizontalmente 
     * en la sopa binaria
     * @throws java.lang.Exception
     */
    public int getCuantasVeces_Horizontal(int decimal) throws Exception{
        
        int cuantasVeces = 0;
        Bit[] binario = pasarBit(decimal);
        for (int i = 0; i < mySopaBinaria.length; i++) {
            cuantasVeces += buscarBinarioHorizontal(binario, i);
        }
        return cuantasVeces;
    }
    
    /**
     * Busca horizontalmente de izquierda a derecha el binario en la sopa binaria
     * @param binario decimal convertido en un array de tipo binario
     * @param fila fila de la sopa binaria
     * @return el numero de veces que se encuentra el binario por fila
     */
    public int buscarBinarioHorizontal(Bit[] binario, int fila) {

        int cont = binario.length;
        int valido = 0;
        Bit[] binarioSolucion = new Bit[binario.length];
        for (int columna = 0, iteradorBinario = 0; columna < mySopaBinaria[fila].length; columna++) {
            if (mySopaBinaria[fila][columna].isValor() == binario[iteradorBinario].isValor()) {
                binarioSolucion[iteradorBinario] = mySopaBinaria[fila][columna];
                cont--;
                iteradorBinario++;
            } else {
                if (iteradorBinario != 0) {
                    columna = Math.max(columna - 1, 0);
                }
                binarioSolucion = new Bit[binario.length];
                iteradorBinario = 0;
                cont = binario.length;
            }
            if (cont == 0) {
                valido++;
                for (int i = columna, j = binarioSolucion.length - 1; i >= j && j >= 0; i--, j--) {
                    solucionSopaBinaria[fila][i] = binarioSolucion[j];
                }
                iteradorBinario = 0;
                cont = binario.length;
            }
        }
        return valido;
    }

       /**
     *Muestra cuantas veces está el decimal verticalmente en la sopa binaria 
     * @param decimal numero en base 10 que ingresa el usuario el cual será convertido 
     * a binario, para su busqueda vertical en la sopa binaria
     * @return retorna cuantas veces se encuentra el decimal verticalmente 
     * en la sopa binaria
     * @throws java.lang.Exception
     */
    public int getCuantasVeces_Vertical(int decimal) throws Exception{
        int cuantasVeces = 0;
        Bit[] binario = pasarBit(decimal);
        for (int j = 0; j < mySopaBinaria[0].length; j++) {
            cuantasVeces += buscarBinarioVertical(binario, j);
        }
        return cuantasVeces;
    }
    
        /**
     * Busca verticalmente de izquierda a derecha el binario en la sopa binaria
     * @param binario decimal convertido en un array de tipo binario
     * @param columna columna de la sopa binaria
     * @return el numero de veces que se encuentra el binario por columna
     */
    public int buscarBinarioVertical(Bit[] binario, int columna){

        int contador = binario.length;
        int valido = 0;
        Bit[] binarioSolucion = new Bit[binario.length];
        for (int fila = 0, iteradorBinario = 0; fila < mySopaBinaria.length; fila++) {
            if (mySopaBinaria[fila][columna].isValor() == binario[iteradorBinario].isValor()) {
                binarioSolucion[iteradorBinario] = mySopaBinaria[fila][columna];
                iteradorBinario++;
                contador--;

            } else {
                if (iteradorBinario != 0) {
                    fila = Math.max(fila - 1, 0);
                }
                binarioSolucion = new Bit[binario.length];
                iteradorBinario = 0;
                contador = binario.length;
            }
            if (contador == 0) {
                valido++;
                for (int i = fila, j = binarioSolucion.length - 1; i >= j && j >= 0; i--, j--) {
                    solucionSopaBinaria[i][columna] = binarioSolucion[j];
                }
                iteradorBinario = 0;
                contador = binario.length;
            }
        }
        return valido;
    }

       /**
     *Muestra cuantas veces está el decimal de forma diagonal en la sopa binaria 
     * update: Recorre las diagonales de izquierda a derecha de la sopa binaria correctamente,
     * pero no se logra buscar y agregar correctamente en la matriz solucionSopaBinaria
     * @param decimal numero en base 10 que ingresa el usuario el cual será convertido 
     * a binario, para su busqueda diagonal en la sopa binaria
     * @return retorna cuantas veces se encuentra el decimal de forma diagonal 
     * en la sopa binaria
     * @throws java.lang.Exception
     */
    public int getCuantasVeces_Diagonal(int decimal) throws Exception{
        int cuantasVeces = 0;
        Bit[] binario = pasarBit(decimal);
        for (int i = 0; i < mySopaBinaria.length; i++) {
            cuantasVeces += buscarBinarioDiagonalForma1(binario, i);
            cuantasVeces += buscarBinarioDiagonalForma2(binario, i);
        }
        return cuantasVeces;
    }
    
    /**
     * Busca el binario en la sopa binaria de forma diagonal de izquierda a derecha 
     * @param binario decimal convertido en un array de tipo binario
     * @param fila fila de sopa binaria
     * @return el numero de veces que se encuentra el binario de forma diagonal
     */
    public int buscarBinarioDiagonalForma1(Bit[] binario, int fila) {
        int cont = binario.length;
        int valido = 0, auxf, auxc;
        Bit[] binarioSolucion = new Bit[binario.length];
        
        for(int f = fila; f<mySopaBinaria.length-1; f++){
            auxf = f;
            auxc = 0;
            for(int c = 0, iteradorBinario = 0; auxc<mySopaBinaria[f].length; c+=0){
               
                if(mySopaBinaria[auxf][auxc].isValor() == binario[iteradorBinario].isValor()){
                    cont--;
                    iteradorBinario++;
                    auxf++;
                    auxc++;
                    if (cont == 0) {
                        valido++;
                        int k = binario.length - 1;
                        for(int i = auxf-1, j = auxc-1; k<binario.length; i++, j++, k++){
                            solucionSopaBinaria[i][j] = binario[k];
                        }
                        auxc = ++c;
                        auxf = f;
                        iteradorBinario = 0;
                        cont = binario.length;
                        binarioSolucion = new Bit[binario.length];
                    }
                    if(auxc==mySopaBinaria[f].length-1)
                        auxc = ++c;
                }
                else{
                    auxf = f;
                    auxc = ++c;
                    cont = binario.length;
                    iteradorBinario = 0;
                    binarioSolucion = new Bit[binario.length];
                }
            }
        }
        return valido;
    }
    /**
     * Busca el binario en la sopa binaria de forma diagonal de derecha a izquierda 
     * @param binario decimal convertido en un array de tipo binario
     * @param fila fila de sopa binaria
     * @return el numero de veces que se encuentra el binario de forma diagonal
     */
    public int buscarBinarioDiagonalForma2(Bit[] binario, int fila) {
        int cont = binario.length;
        int valido = 0, auxf, auxc;
        Bit[] binarioSolucion = new Bit[binario.length];
        
        for(int f = mySopaBinaria.length-1; f<fila ; f--){
            auxf = fila;
            auxc = 0;
            for(int c = 0, iteradorBinario = 0; auxc < mySopaBinaria[f].length; c+=0){
                System.out.println((mySopaBinaria[auxf][auxc].isValor() == binario[iteradorBinario].isValor()) + " " + mySopaBinaria[auxf][auxc].isValor() + " " + binario[iteradorBinario].isValor());
                if(mySopaBinaria[auxf][auxc].isValor() == binario[iteradorBinario].isValor()){
                    cont--;
                    iteradorBinario++;
                    auxf++;
                    auxc++;
                    if (cont == 0) {
                        valido++;
                        int k = binario.length - 1;
                        for(int i = auxf-1, j = auxc-1; k<binario.length; i--, j--, k++){
                            //System.out.println(i + " " + j + " " + binarioSolucion[k].isValor());
                            solucionSopaBinaria[i][j] = binario[k];
                            System.out.print(solucionSopaBinaria[i][j] + "\t");
                        }
                        auxc = ++c;
                        auxf = fila;
                        iteradorBinario = 0;
                        cont = binario.length;
                        binarioSolucion = new Bit[binario.length];
                    }
                    if(auxc==mySopaBinaria[f].length-1)
                        auxc = ++c;
                }
                else{
                    auxf = fila;
                    auxc = ++c;
                    cont = binario.length;
                    iteradorBinario = 0;
                    binarioSolucion = new Bit[binario.length];
                }
            }
            
        }
        System.out.println("");
        return valido;
    }
    
    /**
     *Convierte a bit un numero decimal
     * @param decimal numero en base 10 insertado por el usuario
     * @return el arreglo de bit equivalente a cada decimal
     * @throws java.lang.Exception lanza una execpcion en caso que decimal sea nulo
     */
    
    public Bit[] pasarBit(Integer decimal) throws Exception{
        
        if(decimal == null )
            throw new Exception ("Error nulo, digite el número");
            
        int numero = decimal;
        String binario = "";
        int digito;
        do {
            digito = numero % 2;
            binario = digito + binario;
            numero /= 2;
        } while (numero != 0);
        char[] aCaracteres = binario.toCharArray();

        Bit[] bits = new Bit[aCaracteres.length];
        for (int i = 0; i < aCaracteres.length; i++) {
            if (aCaracteres[i] == 49) {
                bits[i] = new Bit(true);
            } else {
                bits[i] = new Bit(false);
            }
        }
        return bits;
    }
    
    /**
     * Obteniene el valor de la sopa binaria
     * @return un arrray bidimensional con los valores de la sopa binaria
     */
    public Bit[][] getMySopaBinaria() {
        return mySopaBinaria;
    }
    
    /**
     * Obtiene el valor de la solución sopa binaria
     * @return un array bidimensional con los valores de la sopa binaria solución
     */
    public Bit[][] getSolucionSopaBinaria() {
        return solucionSopaBinaria;
    }

    @Override
    public String toString() {
        String msg = "";
        for (Bit filas[] : this.mySopaBinaria) {
            for (Bit valor : filas) {
                msg += valor.isValor() + "\t";
            }
            msg += "\n";
        }
        return msg;
    }

}
