/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Bit;
import Util.LeerSopaExcel;
import Negocio.SopaBinaria;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author julia
 */
public class TestExcel {

    public static void main(String[] args) throws IOException, Exception {
        LeerSopaExcel myExcel = new LeerSopaExcel("src/Datos/sopa.xls", 0);
        String datos[][] = myExcel.getMatriz();
        for (String filas[] : datos) {
            for (String datoColumna : filas) {
                System.out.print(datoColumna + "\t");
            }
            System.out.println("\n");
        }
        SopaBinaria sopaB = new SopaBinaria("src/Datos/sopa.xls");
        sopaB.getCuantasVeces_Horizontal(16);
        sopaB.getCuantasVeces_Vertical(16);
        //sopaB.getCuantasVeces_Diagonal(16);
        try {
            crearInforme_PDF(sopaB);
        } catch (Exception ex) {
            Logger.getLogger(TestExcel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void imprimir(Bit[][] datos) {
        for (Bit filas[] : datos) {
            for (Bit datoColumna : filas) {
                System.out.print(datoColumna + "\t");
            }
            System.out.println("\n");
        }
    }

    public static void crearInforme_PDF(SopaBinaria mySopa) throws Exception {
        Bit[][] sopa = mySopa.getMySopaBinaria();
        Bit[][] resultado = mySopa.getSolucionSopaBinaria();
        imprimir(resultado);
        //1. Crear el objeto que va a form-atear el pdf:
        Document documento = new Document();
        //2. Crear el archivo de almacenamiento--> PDF
        FileOutputStream ficheroPdf = new FileOutputStream("src/pdf_generado/ficheroSalida.pdf");
        //3. Asignar la estructura del pdf al archivo físico:
        PdfWriter.getInstance(documento, ficheroPdf);
        documento.open();

        Paragraph parrafo = new Paragraph();
        parrafo.add("La solución de la sopa Binaria es la siguiente: \n\n");

        //Vamos a crear la tabla: Se debe crear con la cantidad de columnas
        PdfPTable tabla = new PdfPTable(sopa[0].length);
        for (int i = 0; i < sopa.length; i++) {
            for (int j = 0; j < sopa[i].length; j++) {
                String dato = sopa[i][j].toString();
                PdfPCell celda = new PdfPCell(new Phrase(dato));
                if (resultado[i][j] != null && resultado[i][j].isValor() == sopa[i][j].isValor())
                    celda.setBackgroundColor(BaseColor.PINK);
                tabla.addCell(celda);
            }
        }
        documento.add(parrafo);
        documento.add(tabla);
        documento.close();
    }
}
